Shader "Mael/Mri2DSlice"
{
    Properties
    {
        _SliceDirection("_SliceDirection", Int) = 0
    }

    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        Tags { "Queue" = "Transparent" "IgnoreProjector"="True" "RenderType" = "Transparent" "RenderPipeline" = "UniversalPipeline" }
		Blend DstAlpha One
		// DstAlpha : The value of this stage is multiplied by frame buffer source alpha value.
		// One : The value of one - use this to let either the source or the destination color come through fully.
		LOD 500

        Pass
        {
			Cull Back // Front Don’t render polygons that are facing towards the viewer. Used for turning objects inside-out.
			ZWrite Off // Controls whether pixels from this object are written to the depth buffer.
			ZTest Off

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			// Some fixed variable (Compile-time)
			#define RESULT_ADJUSTMENTS 0.005

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                float4 positionOS : POSITION;
            };

            struct Varyings
            {
                float4 positionCS : SV_POSITION; // position in homogenous Clip Space (frostrum cam)
                float4 positionOS  : TEXCOORD0;
            };

			sampler2D  _CameraDepthTexture;

            uniform bool _DisplaySlice;
			uniform float3 _PlanePosition;

			uniform sampler3D _MriData;
            uniform half _Gain;
            uniform half _Gamma;
            uniform half _Mini;

            // To make the Unity shader SRP Batcher compatible, declare all
            // properties related to a Material in a a single CBUFFER block with
            // the name UnityPerMaterial.
            CBUFFER_START(UnityPerMaterial)
                int _SliceDirection;
            CBUFFER_END


			// ----------------------------------------------------
			// Utilities
			// ----------------------------------------------------

			// returns voxel value at a given position
			half2 get_data(float3 voxelPosition, half gain, half gamma, half mini, sampler3D mri_data)
			{
				half value;
                if (_SliceDirection == 2)
                {
                    value = tex3Dlod(mri_data, float4(voxelPosition.x * 0.1 + 0.5, - voxelPosition.z * 0.1 + 0.5, _PlanePosition.y + 0.5, 1)).r; // read MRI. Adding 0.5 to center the data (Plane origin is center of plane, MRI origin is not)
                }
                else if (_SliceDirection == 1)
                {
                    value = tex3Dlod(mri_data, float4(voxelPosition.x * 0.1 + 0.5, _PlanePosition.z + 0.5, voxelPosition.z * 0.1 + 0.5, 1)).r; // read MRI. Adding 0.5 to center the data (Plane origin is center of plane, MRI origin is not)
                }
                else
                {
                    value = tex3Dlod(mri_data, float4(_PlanePosition.x + 0.5, voxelPosition.x * 0.1 + 0.5, voxelPosition.z * 0.1 + 0.5, 1)).r; // read MRI. Adding 0.5 to center the data (Plane origin is center of plane, MRI origin is not)
                }

				// apply resultData adjustments.
				value = pow( max(value - mini, 0), gamma) * RESULT_ADJUSTMENTS;

				// set output voxel rgba
				return half2(value, value * gain);
			}

            // ----------------------------------------------------
			// Vertex program
			// ----------------------------------------------------

            // The vertex shader definition with properties defined in the Varyings
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            Varyings vert(Attributes IN)
            {
                Varyings OUT;

                VertexPositionInputs positionInputs = GetVertexPositionInputs(IN.positionOS.xyz);
                OUT.positionCS = positionInputs.positionCS;
                OUT.positionOS = IN.positionOS;

                return OUT;
            }

            // The fragment shader definition.
            half4 frag(Varyings input) : SV_Target
            {
				// ray march along eye ray
                if (_DisplaySlice)
                {
				    return get_data(input.positionOS.xyz, _Gain, _Gamma, _Mini, _MriData).xxxy;
                }
                else
                {
                    return 0;
                }
            }

            ENDHLSL
        }
    }
}
