Shader "Mael/MriVisualisation"
{
    Properties
    {
		// the data cube
		[NoScaleOffset] _Atlas("Atlas ", 3D) = "" {} // Atlas

		// Slicing
		[Toggle] _Slicer("Slicer", Float) = 0.0
		[Toggle] _SlicerPlane("Slicer Plane", Float) = 0.0
		[Toggle] _SlicerEndoscope("Slicer Endoscope", Float) = 0.0

		// Atlas
		[Toggle] _UseAtlas("Use Atlas", Float) = 0.0
		[Toggle] _HideHead("Hide Head", Float) = 0.0
		[Toggle] _ShowAtlasColours("Show Atlas Colours", Float) = 0.0
    }

    SubShader
    {
        // SubShader Tags define when and under which conditions a SubShader block or
        // a pass is executed.
        Tags { "RenderType" = "Transparent" "Queue"="Transparent" "IgnoreProjector"="True" "RenderPipeline" = "UniversalPipeline" }

        Blend SrcAlpha OneMinusSrcAlpha
		// DstAlpha : The value of this stage is multiplied by frame buffer source alpha value.
		// One : The value of one - use this to let either the source or the destination color come through fully.

		Lighting Off
		LOD 500

        Pass
        {
			Cull Front // Front: Don’t render polygons that are facing towards the viewer. Used for turning objects inside-out.
			ZWrite On // Controls whether pixels from this object are written to the depth buffer.
			// ZTest Off

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			// #pragma shader_feature _ALPHATEST_ON
            // #pragma shader_feature _ALPHAPREMULTIPLY_ON

            // The Core.hlsl file contains definitions of frequently used HLSL
            // macros and functions, and also contains #include references to other
            // HLSL files (for example, Common.hlsl, SpaceTransforms.hlsl, etc.).
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			// Some fixed variable (Compile-time)
			#define STEP_CNT 1024 // Can be reduced if shader is too resources-heavy
			#define RESULT_ADJUSTMENTS 0.005
			#define SQR_MAX_DISTANCE_SLICER 1

            // The structure definition defines which variables it contains.
            // This example uses the Attributes structure as an input structure in
            // the vertex shader.
            struct Attributes
            {
                // The positionOS variable contains the vertex positions in object
                // space.
                float4 positionOS : POSITION;
            };

            struct Varyings
            {
                float4 positionCS : SV_POSITION; // position in homogenous Clip Space (frostrum cam)
                float3 positionVS : TEXCOORD0; // position in View Space
				float3 viewPositionOS : TEXCOORD1; // ray origin in Object Space
				float3 rayDirectionOS : TEXCOORD2; // ray direction in Object Space
            };

			// sampler2D  _CameraDepthTexture;

			uniform float3 _PlanePosition;
			uniform float3 _PlaneNormal;
			uniform float3 _EndoscopePosition;
			uniform float3 _EndoscopeNormal;
			uniform half _EndoscopeLength;
			uniform half _EndoscopeWidth;
			uniform sampler3D _MriData;
            uniform half _Gain;
        	uniform half _Gamma;
            uniform half _Mini;

			uniform StructuredBuffer<half4> _Colors;

            // To make the Unity shader SRP Batcher compatible, declare all
            // properties related to a Material in a a single CBUFFER block with
            // the name UnityPerMaterial.
            CBUFFER_START(UnityPerMaterial)
				sampler3D _Atlas;
            CBUFFER_END

			CBUFFER_START(UnityPerFrame)
				bool _Slicer;
				bool _SlicerPlane;
				bool _SlicerEndoscope;

				bool _UseAtlas;
				bool _HideHead;
				bool _ShowAtlasColours;
            CBUFFER_END


			// ----------------------------------------------------
			// Utilities
			// ----------------------------------------------------

			// Calculates intersection between a ray and a box, and give points
			bool IntersectBox(float3 rayOrigin, float3 rayDirection, half3 boxMin, half3 boxMax, out half tNear, out half tFar)
			{
				// compute intersection of ray with all six bbox planes
				half3 invertedRay = rcp(rayDirection); // ~= 1 / rayDirection
				half3 tBot = (boxMin.xyz - rayOrigin) * invertedRay;
				half3 tTop = (boxMax.xyz - rayOrigin) * invertedRay;

				// re-order intersections to find smallest and largest on each axis
				half3 tMin = min(tTop, tBot);
				half3 tMax = max(tTop, tBot);
				// find the largest tMin and the smallest tMax
				half largest_tMin = max( max(tMin.x, tMin.y), tMin.z);
				half smallest_tMax = min( min(tMax.x, tMax.y), tMax.z);
				// check for hit
				bool hit = (largest_tMin <= smallest_tMax);
				tNear = largest_tMin;
				tFar = smallest_tMax;
				return hit;
			}

			half2 get_mri_data(float4 mri_position, sampler3D mri_data)
			{
				half value = tex3Dlod(mri_data, mri_position).r; // read MRI. Adding 0.5 to center the data (Cube origin is center of cube, MRI origin is not)

				// apply resultData adjustments.
				value = pow( max(value - _Mini, 0), _Gamma) * RESULT_ADJUSTMENTS;

				// set output voxel rgba
				return half2(value, value * _Gain);
			}

			// returns voxel value at a given position
			half2 get_sliced_data(float3 voxelPosition, float4 mri_position, sampler3D mri_data)
			{
				bool draw = true;

				if (_Slicer)
				{
					// plane cutaway
					if (_SlicerPlane && dot(_PlanePosition, _PlanePosition) < SQR_MAX_DISTANCE_SLICER)
					{
						draw = dot(voxelPosition - _PlanePosition, _PlaneNormal) > 0;
					}
					// cylinder cutaway
					if (_SlicerEndoscope && !(_EndoscopePosition.x == 0 && _EndoscopePosition.y == 0 && _EndoscopePosition.z == 0))
					{
						half3 delta = (voxelPosition - _EndoscopePosition);
						half p = dot(delta, _EndoscopeNormal);
						draw = draw && (p > _EndoscopeLength || length(delta - p * _EndoscopeNormal) > _EndoscopeWidth);
					}
				}

				return draw ? get_mri_data(mri_position, mri_data) : 0;
			}

            // perform ray marching
			half4 ray_march(float3 rayOrigin, float3 rayDirection, half tNear, half tFar, sampler3D data, out half t)
			{
				const half step = 1.7322 / STEP_CNT; // cf. cube diagonal sqrt(3)
				const half stepDithering = step * 0.01;

				t = tNear;
				float3 currentVoxelAlongRay;
				half4 ray_color = 0;
				half4 voxel_color = 0;

				// ray march
				while (t < tFar && ray_color.a <= 0.99)
				{
					currentVoxelAlongRay = rayOrigin + (rayDirection * t); // sampling position.
					float4 mri_position = float4(currentVoxelAlongRay.x + 0.5, currentVoxelAlongRay.z + 0.5, currentVoxelAlongRay.y + 0.5, 1);

					voxel_color = get_sliced_data(currentVoxelAlongRay, mri_position, data).xxxy;

					if (_UseAtlas)
					{
						half index = tex3Dlod(_Atlas, mri_position).r; // read Atlas
						if (_ShowAtlasColours)
						{
							if (index > 0 && index < 15000)
							{
								// voxel_color.rgb = half3(1, 0.1 , 0.1) * voxel_color.rgb;
								voxel_color.rgb = _Colors[index].rgb * voxel_color.rgb;
							}
						}
						if (_HideHead)
						{
							voxel_color.a = index == 0 ? 0 : voxel_color.a;
						}
					}

					ray_color.rgb = voxel_color.a > 0.01 ? lerp(ray_color.rgb, voxel_color.rgb, min(voxel_color.a, 1 - ray_color.a) / saturate(ray_color.a + voxel_color.a)) : ray_color.rgb; // if a not > 0.01 do nothing
					ray_color.a = voxel_color.a > 0.01 ? ray_color.a + voxel_color.a : ray_color.a; // if a not > 0.01 do nothing

					// march on with depth dithering to avoid banding artifacts
					t += step + stepDithering * frac(sin(dot(rayDirection.xy, float2(12.9898, 78.233)) * 43758.5453));
				}
				if (ray_color.a > 0.99)
				{
					ray_color.a = 1;
				}

				return ray_color;
			}

            // ----------------------------------------------------
			// Vertex program
			// ----------------------------------------------------

            // The vertex shader definition with properties defined in the Varyings
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
            Varyings vert(Attributes IN)
            {
                Varyings OUT;

                // calculate position on screen
                VertexPositionInputs positionInputs = GetVertexPositionInputs(IN.positionOS.xyz);
                OUT.positionVS = positionInputs.positionVS;
                OUT.positionCS = positionInputs.positionCS;

                // calculate eye ray in object space
                OUT.rayDirectionOS = TransformWorldToObjectDir(-GetWorldSpaceViewDir(positionInputs.positionWS), false); // WS to normalized Object Space
				OUT.viewPositionOS = TransformWorldToObject(GetCurrentViewPosition());

                return OUT;
            }

            // The fragment shader definition.
            // float4 frag(Varyings input, out float depth : SV_Depth) : SV_Target
            float4 frag(Varyings input) : SV_Target
            {
				// calculate eye-ray intersection with cube bounding box
				half tNear, tFar;
				bool hit = IntersectBox(input.viewPositionOS, input.rayDirectionOS, half3(-0.5, -0.5, -0.5), half3(0.5, 0.5, 0.5), tNear, tFar);
				if (!hit) discard; // exit if not in sight

				if (tNear < 0) tNear = 0;

				// query depth buffer
				// float tScene = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, input.positionVS.xyzx).r, _ZBufferParams);
				// tScene *= length(float3(input.positionVS.xy/ input.positionVS.w - 0.5,1)); // convert to ray length
				// if (tScene < tNear) discard; // exit if scene object nearer

				// ray march along eye ray
				half t;
				half4 rayMarchOutput = ray_march(input.viewPositionOS, input.rayDirectionOS, tNear, tFar, _MriData, t);
				
				// if (rayMarchOutput.a == 1)
				// {
				// 	// depth = t * _ZBufferParams.y;
				// 	// depth = 1 - t * _ProjectionParams.y * _ProjectionParams.w;
				// 	float l = lerp(_ProjectionParams.y, _ProjectionParams.z, t);
				// }
				// else
				// {
				// 	depth = 0;
				// }

				return rayMarchOutput;
            }

            ENDHLSL
        }
    }
}
