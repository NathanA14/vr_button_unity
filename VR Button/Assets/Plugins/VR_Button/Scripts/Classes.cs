using UnityEngine;

namespace VRButtonTools
{
    public class VRButtonColors
    {
        Color _unactiveColor, _pressedColor, _standardColor, _touchedColor;
    }

    public class VRButtonMaterial
    {
        Material _unactiveMaterial, _pressedMaterial, _standardMaterial, _touchedMaterial;
    }
}
