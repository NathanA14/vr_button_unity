using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAction : MonoBehaviour
{
    [SerializeField]
    private Color[] colors;
    [SerializeField]
    private Material mat;
    private Color color;

    private void Awake()
    {
        color = mat.color;
    }

    public void ChangeColor()
    {
        var len = colors.Length;
        if (len < 2)
        {
            Debug.LogError("colors must have at least 2 elements");
            return;
        }

        while(color == mat.color)
        {
            color = colors[Random.Range(0, len)];
        }
        mat.color = color;
    }
}

