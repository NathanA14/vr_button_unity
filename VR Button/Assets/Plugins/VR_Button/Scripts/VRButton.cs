using System.Collections;
using UnityEngine;
using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using VRButtonTools;

[AddComponentMenu("UI/Button", 30)]
public class VRButton : MonoBehaviour
{
    [SerializeField]
    GameObject _baseButton, _movableButton;
    public bool isInteractable = true;
    bool isPressed, isTouched = false;

    public bool useTextures = false;
    public VRButtonColors _colors = new VRButtonColors();
    //public VRButtonMaterial _materials = new VRButtonMaterial();

    GameObject _pusher = null;
    float YmaxButton = 0f;

    [Serializable]
    public class VRButtonClickedEvent : UnityEvent { }

    // Event delegates triggered on click.
    [FormerlySerializedAs("onClick")]
    [SerializeField]
    private VRButtonClickedEvent m_OnClick = new VRButtonClickedEvent();

    protected VRButton()
    { }

    private void Start()
    {
        YmaxButton = _movableButton.transform.position.y;
    }

    private void Update()
    {
        if (!isInteractable)
        {
            isPressed = isTouched = false;
        }

        if (isTouched)
        {
            Vector3 pos = _movableButton.transform.position;
            _movableButton.transform.position = new Vector3(pos.x, Mathf.Min(pos.y,YmaxButton), pos.z);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        _pusher = other.gameObject;
        isTouched = true;
    }

    public VRButtonClickedEvent onClick
    {
        get { return m_OnClick; }
        set { m_OnClick = value; }
    }

    private void Press()
    {
        isPressed = true;
        UISystemProfilerApi.AddMarker("VRButton.onClick", this);
        m_OnClick.Invoke();
    }

    private void OnButtonTouched()
    {
        //change color
    }

    private void OnSubmit()
    {
        Press();
        //changeColor
    }

    private void OnButtonLeft()
    {
        
    }

}


