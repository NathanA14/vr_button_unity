using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandController : MonoBehaviour
{
    public InputDeviceCharacteristics controllerType;
    public InputDevice thisController;

    public Animator animationController;
    private bool isControllerFound;

	private bool isCloseToButton = false;
	private bool justExitedTrigger = false;
	private float t = 0;
	// Start is called before the first frame update
	void Start()
    {
      animationController = GetComponent<Animator>();
      Initialise();
    }

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "CloseButtonZone")
			t = 0;
	}

	private void OnTriggerStay(Collider other)
	{
		if (other.tag == "CloseButtonZone")
		{
			isCloseToButton = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "CloseButtonZone")
		{
			isCloseToButton = false;
			t = 0;
			//justExitedTrigger = true;
		}
	}

	private void Initialise()
    {
        List<InputDevice> XRDevices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(controllerType, XRDevices);

        if(XRDevices.Count.Equals(0))
        {
            Debug.Log("No XR devices found");
        }
        else    
        {
            thisController = XRDevices[0];
            isControllerFound = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isControllerFound)
        {
            Initialise();
        }
        else if (!isCloseToButton)
        {
            if(thisController.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
            {
                animationController.SetFloat("Trigger", triggerValue);
            }

            if(thisController.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
            {
                animationController.SetFloat("Grip", gripValue);
            }
        }
		/*else if (justExitedTrigger)
		{
			var value = Mathf.Lerp(1, 0, t);

			animationController.SetFloat("Trigger", value);
			animationController.SetFloat("Grip", value);
			t += 6f * Time.deltaTime;
			if (value < 0.01f)
				justExitedTrigger = false;
		}*/
		else
		{
			t += 6f * Time.deltaTime;
			var value =  Mathf.Lerp(0, 1, t);

			animationController.SetFloat("Trigger", value);
			animationController.SetFloat("Grip", value);
		}
	}
}
