using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit.UI;

namespace UnityEngine.XR.Interaction.Toolkit
{
	/// <summary>
	/// Interactor used for directly interacting with interactables that are touching. This is handled via trigger volumes
	/// that update the current set of valid targets for this interactor. This component must have a collision volume that is
	/// set to be a trigger to work.
	/// </summary>
	[DisallowMultipleComponent]
	[AddComponentMenu("XR/XR Direct Interactor With UI")]
	public class XRDirectInteractorWithUI : XRDirectInteractor, IUIInteractor
	{

		[SerializeField]
		bool m_EnableUIInteraction = true;
		/// <summary>
		/// Gets or sets whether this interactor is able to affect UI.
		/// </summary>
		public bool enableUIInteraction
		{
			get => m_EnableUIInteraction;
			set
			{
				if (m_EnableUIInteraction != value)
				{
					m_EnableUIInteraction = value;
					RegisterOrUnregisterXRUIInputModule();
				}
			}
		}

		XRUIInputModule m_InputModule;
		XRUIInputModule m_RegisteredInputModule;


		/// <inheritdoc />
		protected override void OnEnable()
		{
			base.OnEnable();

			if (m_EnableUIInteraction)
				RegisterWithXRUIInputModule();
		}

		/// <inheritdoc />
		protected override void OnDisable()
		{
			base.OnDisable();

			if (m_EnableUIInteraction)
				UnregisterFromXRUIInputModule();
		}

		/// <inheritdoc />
		public virtual void UpdateUIModel(ref TrackedDeviceModel model)
		{
			if (!isActiveAndEnabled)
				return;

			model.position = transform.position;
			model.orientation = transform.rotation;
			model.select = isUISelectActive;
		}

		/// <inheritdoc />
		public bool TryGetUIModel(out TrackedDeviceModel model)
		{
			if (m_InputModule != null)
			{
				return m_InputModule.GetTrackedDeviceModel(this, out model);
			}

			model = new TrackedDeviceModel(-1);
			return false;
		}

		/// <summary>
		/// Register with or unregister from the Input Module (if necessary).
		/// </summary>
		/// <remarks>
		/// If this behavior is not active and enabled, this function does nothing.
		/// </remarks>
		void RegisterOrUnregisterXRUIInputModule()
		{
			if (!isActiveAndEnabled || !Application.isPlaying)
				return;

			if (m_EnableUIInteraction)
				RegisterWithXRUIInputModule();
			else
				UnregisterFromXRUIInputModule();
		}

		void FindOrCreateXRUIInputModule()
		{
			var eventSystem = FindObjectOfType<EventSystem>();
			if (eventSystem == null)
				eventSystem = new GameObject("EventSystem", typeof(EventSystem)).GetComponent<EventSystem>();
			else
			{
				// Remove the Standalone Input Module if already implemented, since it will block the XRUIInputModule
				var standaloneInputModule = eventSystem.GetComponent<StandaloneInputModule>();
				if (standaloneInputModule != null)
					Destroy(standaloneInputModule);
			}

			m_InputModule = eventSystem.GetComponent<XRUIInputModule>();
			if (m_InputModule == null)
				m_InputModule = eventSystem.gameObject.AddComponent<XRUIInputModule>();
		}

		/// <summary>
		/// Register with the <see cref="XRUIInputModule"/> (if necessary).
		/// </summary>
		/// <seealso cref="UnregisterFromXRUIInputModule"/>
		void RegisterWithXRUIInputModule()
		{
			if (m_InputModule == null)
				FindOrCreateXRUIInputModule();

			if (m_RegisteredInputModule == m_InputModule)
				return;

			UnregisterFromXRUIInputModule();

			m_InputModule.RegisterInteractor(this);
			m_RegisteredInputModule = m_InputModule;
		}

		/// <summary>
		/// Unregister from the <see cref="XRUIInputModule"/> (if necessary).
		/// </summary>
		/// <seealso cref="RegisterWithXRUIInputModule"/>
		void UnregisterFromXRUIInputModule()
		{
			if (m_RegisteredInputModule != null)
				m_RegisteredInputModule.UnregisterInteractor(this);

			m_RegisteredInputModule = null;
		}
	}
}
